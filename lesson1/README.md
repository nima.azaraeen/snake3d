# lesson 1: The basics of 3D
In this lesson we are going to create a moving box
## step 1 : Create the base
Lets start with normal `HTML` and download `threejs` from their [website](https://threejs.org/build/three.js) and put in the folder `js` 
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Snake 3D</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<script src="js/three.js"></script>
		<script>
			// Our Javascript will go here.
		</script>
	</body>
</html>
```

## step 2: add the scene, camera and renderer
```javascript
const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setClearColor("#AFAFAF");
renderer.setSize( window.innerWidth, window.innerHeight );

document.body.appendChild( renderer.domElement );

renderer.render(scene,camera);
```

## step 3: adding a sample object
```javascript
const obj1Geo = new THREE.BoxGeometry(1,10,10);
const obj1Mat = new THREE.MeshLambertMaterial({color: 0x00FF00});
let obj1 = new THREE.Mesh(obj1Geo, obj1Mat);

scene.add(obj1);
```
then we need to add a position to our camera
```javascript
camera.position.z = 3;
```
## step 4: add a light source
```javascript
const light1 = new THREE.PointLight(0xFFFFFF, 1, 500);
light1.position.set(10,0,25);
scene.add(light1);
```
## step 5: animation
```javascript
const animate = function () {
	requestAnimationFrame( animate );
	obj1.rotation.x += 0.01;
	renderer.render( scene, camera );
};

animate();
```
## well done
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Snake 3D</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<script src="js/three.js"></script>
		<script>
			const scene = new THREE.Scene();

			const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
			camera.position.z = 3;

			const renderer = new THREE.WebGLRenderer({antialias: true});
			renderer.setClearColor("#AFAFAF");
			renderer.setSize( window.innerWidth, window.innerHeight );

			const obj1Geo = new THREE.BoxGeometry(1,1,1);
			const obj1Mat = new THREE.MeshLambertMaterial({color: 0x00FF00});
			const obj1 = new THREE.Mesh(obj1Geo, obj1Mat);

			scene.add(obj1);

			const light1 = new THREE.PointLight(0xFFFFFF, 1, 500);
			light1.position.set(10,0,25);
			scene.add(light1);


			document.body.appendChild( renderer.domElement );

			const animate = function () {
				requestAnimationFrame( animate );
				obj1.rotation.x += 0.01;
				renderer.render( scene, camera );
			};

			animate();
		</script>
	</body>
</html>
```
