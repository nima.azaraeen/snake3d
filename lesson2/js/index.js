const scene = new THREE.Scene();


camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
camera.position.set( 200, 300, 500 );
camera.lookAt( 0, 0, 0 );

const renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setClearColor("#AFAFAF");
renderer.setSize( window.innerWidth, window.innerHeight );

var geometry = new THREE.PlaneGeometry( 300, 300 );
var material = new THREE.MeshLambertMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
var plane = new THREE.Mesh( geometry, material );
plane.rotateX(90*((Math.PI)/180));
scene.add( plane );

var gridHelper = new THREE.GridHelper( 300, 30 );
scene.add( gridHelper );

var ambientLight = new THREE.AmbientLight( 0x606060 );
scene.add( ambientLight );

var directionalLight = new THREE.DirectionalLight( 0xffffff );
directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
scene.add( directionalLight );


let Snake = [
    {x:250,z:250},
    {x:240,z:250},
    {x:230,z:250},
    {x:220,z:250},
    {x:210,z:250},
];

function drawSquare(xCoordinate, zCoordinate, cubeColor){
    var geometry = new THREE.BoxGeometry( 9, 9, 9 );
	var material = new THREE.MeshLambertMaterial( {color: cubeColor} );
	var cube = new THREE.Mesh( geometry, material );
	cube.position.y +=5;
	cube.position.x = xCoordinate-150+5;
	cube.position.z = zCoordinate-150+5;
	scene.add( cube );
};

function showSnake(){
	Snake.forEach(element => drawSquare(element.x , element.z, 0x33ee00));
};


document.body.appendChild( renderer.domElement );

const animate = function () {
	requestAnimationFrame( animate );
	showSnake();
	renderer.render( scene, camera );
};

animate();